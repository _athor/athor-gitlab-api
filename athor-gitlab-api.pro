#---------------------------------------------------
# Project created by kielsonzinn 2020-03-26T20:11:00
#---------------------------------------------------

QT -= gui
QT += network

TEMPLATE = lib
DEFINES += ATHORGITLABAPI_LIBRARY

CONFIG(debug, debug|release) {
    DESTDIR = build/debug
}
CONFIG(release, debug|release) {
    DESTDIR = build/release
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.u

SOURCES += \
    data/athorgitlabmergerequestdiscussionmodel.cpp \
    data/athorgitlabmergerequestdiscussionnoteauthormodel.cpp \
    data/athorgitlabmergerequestdiscussionnotemodel.cpp \
    data/athorgitlabmergerequestmodel.cpp \
    data/athorgitlabmergerequestversiondiffmodel.cpp \
    data/athorgitlabmergerequestversionmodel.cpp \
    request/athorgitlabmergerequestdiscussionrequest.cpp \
    request/athorgitlabmergerequestrequest.cpp \
    request/athorgitlabmergerequestversionrequest.cpp \
    request/athorgitlabrequest.cpp

HEADERS += \
    athor-gitlab-api_global.h \
    data/athorgitlabmergerequestdiscussionmodel.h \
    data/athorgitlabmergerequestdiscussionnoteauthormodel.h \
    data/athorgitlabmergerequestdiscussionnotemodel.h \
    data/athorgitlabmergerequestmodel.h \
    data/athorgitlabmergerequestversiondiffmodel.h \
    data/athorgitlabmergerequestversionmodel.h \
    request/athorgitlabmergerequestdiscussionrequest.h \
    request/athorgitlabmergerequestrequest.h \
    request/athorgitlabmergerequestversionrequest.h \
    request/athorgitlabrequest.h
