#include "athorgitlabmergerequestdiscussionmodel.h"

#include <QJsonArray>
#include <QJsonObject>

#include "athorgitlabmergerequestdiscussionnotemodel.h"

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestDiscussionModel::AthorGitlabMergeRequestDiscussionModel() {}

QString AthorGitlabMergeRequestDiscussionModel::id() const {
    return _id;
}

void AthorGitlabMergeRequestDiscussionModel::setId( const QString& id ) {
    _id = id;
}

QList<AthorGitlabMergeRequestDiscussionNoteModel*> AthorGitlabMergeRequestDiscussionModel::notes() const {
    return _notes;
}

void AthorGitlabMergeRequestDiscussionModel::setNotes( const QList<AthorGitlabMergeRequestDiscussionNoteModel*>& notes ) {
    _notes = notes;
}

void AthorGitlabMergeRequestDiscussionModel::fromJson( const QJsonObject& json ) {
    _id = json["id"].toString();

    for ( QJsonValue object : json["notes"].toArray() ) {
        QJsonObject objectNote = object.toObject();
        if ( objectNote["type"].toString() != "DiscussionNote" ) {
            continue;
        }

        AthorGitlabMergeRequestDiscussionNoteModel* note = new AthorGitlabMergeRequestDiscussionNoteModel();
        note->fromJson( objectNote );
        _notes.append( note );
    }

}

ATHOR_GITLAB_API_END_NAMESPACE
