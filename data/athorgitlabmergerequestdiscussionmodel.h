#ifndef ATHORGITLABMERGEREQUESTDISCUSSIONMODEL_H
#define ATHORGITLABMERGEREQUESTDISCUSSIONMODEL_H

#include <QList>
#include <QString>

#include <athor-gitlab-api_global.h>

class QJsonObject;

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestDiscussionNoteModel;

class AthorGitlabMergeRequestDiscussionModel {

public:
    AthorGitlabMergeRequestDiscussionModel();

    QString id() const;
    void setId( const QString& id );

    QList<AthorGitlabMergeRequestDiscussionNoteModel*> notes() const;
    void setNotes( const QList<AthorGitlabMergeRequestDiscussionNoteModel*>& notes );

    void fromJson( const QJsonObject& json );

private:
    QString _id = "";
    QList<AthorGitlabMergeRequestDiscussionNoteModel*> _notes = {};

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTDISCUSSIONMODEL_H
