#include "athorgitlabmergerequestdiscussionnoteauthormodel.h"

#include <QJsonObject>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestDiscussionNoteAuthorModel::AthorGitlabMergeRequestDiscussionNoteAuthorModel() {}

QString AthorGitlabMergeRequestDiscussionNoteAuthorModel::name() const {
    return _name;
}

void AthorGitlabMergeRequestDiscussionNoteAuthorModel::setName( const QString& name ) {
    _name = name;
}

void AthorGitlabMergeRequestDiscussionNoteAuthorModel::fromJson( const QJsonObject& json ) {
    _id = json["id"].toInt();
    _name = json["name"].toString();
    _username = json["username"].toString();
}

int AthorGitlabMergeRequestDiscussionNoteAuthorModel::id() const {
    return _id;
}

void AthorGitlabMergeRequestDiscussionNoteAuthorModel::setId( int id ) {
    _id = id;
}

QString AthorGitlabMergeRequestDiscussionNoteAuthorModel::username() const {
    return _username;
}

void AthorGitlabMergeRequestDiscussionNoteAuthorModel::setUsername( const QString& username ) {
    _username = username;
}

ATHOR_GITLAB_API_END_NAMESPACE
