#ifndef ATHORGITLABMERGEREQUESTDISCUSSIONNOTEAUTHORMODEL_H
#define ATHORGITLABMERGEREQUESTDISCUSSIONNOTEAUTHORMODEL_H

#include <QString>

#include <athor-gitlab-api_global.h>

class QJsonObject;

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestDiscussionNoteAuthorModel {

public:
    AthorGitlabMergeRequestDiscussionNoteAuthorModel();

    QString name() const;
    void setName( const QString& name );

    void fromJson( const QJsonObject& json );

    int id() const;
    void setId( int id );

    QString username() const;
    void setUsername( const QString& username );

private:
    int _id = 0;
    QString _name = "";
    QString _username = "";

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTDISCUSSIONNOTEAUTHORMODEL_H
