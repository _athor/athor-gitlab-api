#include "athorgitlabmergerequestdiscussionnotemodel.h"

#include <QJsonObject>

#include "athorgitlabmergerequestdiscussionnoteauthormodel.h"

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestDiscussionNoteModel::AthorGitlabMergeRequestDiscussionNoteModel() {}

int AthorGitlabMergeRequestDiscussionNoteModel::id() const {
    return _id;
}

void AthorGitlabMergeRequestDiscussionNoteModel::setId( int id ) {
    _id = id;
}

QString AthorGitlabMergeRequestDiscussionNoteModel::body() const {
    return _body;
}

void AthorGitlabMergeRequestDiscussionNoteModel::setBody( const QString& body ) {
    _body = body;
}

AthorGitlabMergeRequestDiscussionNoteAuthorModel* AthorGitlabMergeRequestDiscussionNoteModel::author() const {
    return _author;
}

void AthorGitlabMergeRequestDiscussionNoteModel::setAuthor( AthorGitlabMergeRequestDiscussionNoteAuthorModel* author ) {
    _author = author;
}

void AthorGitlabMergeRequestDiscussionNoteModel::fromJson( const QJsonObject& json ) {
    _id = json["id"].toInt();
    _body = json["body"].toString();
    _resolved = json["resolved"].toBool();

    _author = new AthorGitlabMergeRequestDiscussionNoteAuthorModel();
    _author->fromJson( json["author"].toObject() );
}

bool AthorGitlabMergeRequestDiscussionNoteModel::resolved() const {
    return _resolved;
}

void AthorGitlabMergeRequestDiscussionNoteModel::setResolved( bool resolved ) {
    _resolved = resolved;
}

ATHOR_GITLAB_API_END_NAMESPACE
