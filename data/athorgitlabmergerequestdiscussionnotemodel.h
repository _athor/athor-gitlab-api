#ifndef ATHORGITLABMERGEREQUESTDISCUSSIONNOTEMODEL_H
#define ATHORGITLABMERGEREQUESTDISCUSSIONNOTEMODEL_H

#include <QString>

#include <athor-gitlab-api_global.h>

class QJsonObject;

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestDiscussionNoteAuthorModel;

class AthorGitlabMergeRequestDiscussionNoteModel {

public:
    AthorGitlabMergeRequestDiscussionNoteModel();

    int id() const;
    void setId( int id );

    QString body() const;
    void setBody( const QString& body );

    AthorGitlabMergeRequestDiscussionNoteAuthorModel* author() const;
    void setAuthor( AthorGitlabMergeRequestDiscussionNoteAuthorModel* author );

    void fromJson( const QJsonObject& json );

    bool resolved() const;
    void setResolved( bool resolved );

private:
    int _id = 0;
    QString _body = "";
    bool _resolved = false;
    AthorGitlabMergeRequestDiscussionNoteAuthorModel* _author = nullptr;

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTDISCUSSIONNOTEMODEL_H
