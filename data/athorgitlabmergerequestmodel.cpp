#include "athorgitlabmergerequestmodel.h"

#include <QJsonObject>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestModel::AthorGitlabMergeRequestModel() = default;
AthorGitlabMergeRequestModel::~AthorGitlabMergeRequestModel() = default;

int AthorGitlabMergeRequestModel::id() const {
    return _id;
}

void AthorGitlabMergeRequestModel::setId( int id ) {
    _id = id;
}

int AthorGitlabMergeRequestModel::iid() const {
    return _iid;
}

void AthorGitlabMergeRequestModel::setIid( int iid ) {
    _iid = iid;
}

int AthorGitlabMergeRequestModel::projectId() const {
    return _projectId;
}

void AthorGitlabMergeRequestModel::setProjectId( int projectId ) {
    _projectId = projectId;
}

QString AthorGitlabMergeRequestModel::title() const {
    return _title;
}

void AthorGitlabMergeRequestModel::setTitle( const QString& title ) {
    _title = title;
}

QString AthorGitlabMergeRequestModel::targetBranch() const {
    return _targetBranch;
}

void AthorGitlabMergeRequestModel::setTargetBranch( const QString& targetBranch ) {
    _targetBranch = targetBranch;
}

QString AthorGitlabMergeRequestModel::sourceBranch() const {
    return _sourceBranch;
}

void AthorGitlabMergeRequestModel::setSourceBranch( const QString& sourceBranch ) {
    _sourceBranch = sourceBranch;
}

void AthorGitlabMergeRequestModel::fromJson( const QJsonObject& json ) {

    _id = json["id"].toInt();
    _iid = json["iid"].toInt();
    _projectId = json["project_id"].toInt();
    _title = json["title"].toString();
    _sourceBranch = json["source_branch"].toString();
    _targetBranch = json["target_branch"].toString();

}

ATHOR_GITLAB_API_END_NAMESPACE
