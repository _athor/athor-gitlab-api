#ifndef ATHORGITLABMERGEREQUESTMODEL_H
#define ATHORGITLABMERGEREQUESTMODEL_H

#include <QString>

#include <athor-gitlab-api_global.h>

class QJsonObject;

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestModel {

public:
    AthorGitlabMergeRequestModel();
    ~AthorGitlabMergeRequestModel();

    int id() const;
    void setId( int id );

    int iid() const;
    void setIid( int iid );

    int projectId() const;
    void setProjectId( int projectId );

    QString title() const;
    void setTitle( const QString& title );

    QString targetBranch() const;
    void setTargetBranch( const QString& targetBranch );

    QString sourceBranch() const;
    void setSourceBranch( const QString& sourceBranch );

    void fromJson( const QJsonObject& json );

private:
    int _id = 0;
    int _iid = 0;
    int _projectId = 0;
    QString _title = "";
    QString _sourceBranch = "";
    QString _targetBranch = "";

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTMODEL_H
