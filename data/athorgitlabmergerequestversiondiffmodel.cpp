#include "athorgitlabmergerequestversiondiffmodel.h"

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestVersionDiffModel::AthorGitlabMergeRequestVersionDiffModel() {}

QString AthorGitlabMergeRequestVersionDiffModel::newPath() const {
    return _newPath;
}

void AthorGitlabMergeRequestVersionDiffModel::setNewPath( const QString& newPath ) {
    _newPath = newPath;
}

void AthorGitlabMergeRequestVersionDiffModel::fromJson( const QJsonObject& json ) {
    _newPath = json["new_path"].toString();
}

ATHOR_GITLAB_API_END_NAMESPACE
