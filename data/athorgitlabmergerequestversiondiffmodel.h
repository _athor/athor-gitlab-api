#ifndef ATHORGITLABMERGEREQUESTVERSIONDIFFMODEL_H
#define ATHORGITLABMERGEREQUESTVERSIONDIFFMODEL_H

#include <QString>
#include <QJsonObject>

#include <athor-gitlab-api_global.h>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestVersionDiffModel {

public:
    AthorGitlabMergeRequestVersionDiffModel();

    QString newPath() const;
    void setNewPath( const QString& newPath );

    void fromJson( const QJsonObject& json );

private:
    QString _newPath = "";

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTVERSIONDIFFMODEL_H
