#include "athorgitlabmergerequestversionmodel.h"

#include <QJsonObject>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestVersionModel::AthorGitlabMergeRequestVersionModel() {}

int AthorGitlabMergeRequestVersionModel::id() const {
    return _id;
}

void AthorGitlabMergeRequestVersionModel::setId( int id ) {
    _id = id;
}

void AthorGitlabMergeRequestVersionModel::fromJson( const QJsonObject& json ) {
    _id = json["id"].toInt();
}

ATHOR_GITLAB_API_END_NAMESPACE
