#ifndef ATHORGITLABMERGEREQUESTVERSIONMODEL_H
#define ATHORGITLABMERGEREQUESTVERSIONSMODEL_H

#include <athor-gitlab-api_global.h>

class QJsonObject;

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestVersionModel {

public:
    AthorGitlabMergeRequestVersionModel();

    int id() const;
    void setId( int id );

    void fromJson( const QJsonObject& json );

private:
    int _id = 0;

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTVERSIONMODEL_H
