#include "athorgitlabmergerequestdiscussionrequest.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

QList<AthorGitlabMergeRequestDiscussionModel*> AthorGitlabMergeRequestDiscussionRequest::getListByMerge( const int& projectId, const int& mergeIid ) {

    QString url = _url.url();
    url = url.append( "/api/v4/projects/" );
    url = url.append( QString::number( projectId ) );
    url = url.append( "/merge_requests/" );
    url = url.append( QString::number( mergeIid ) );
    url = url.append( "/discussions" );

    _url = QUrl( url );
    QJsonArray json = AthorGitlabRequest::get().array();

    QList<AthorGitlabMergeRequestDiscussionModel*> response = {};

    for ( QJsonValue object : json ) {
        AthorGitlabMergeRequestDiscussionModel* discussion = new AthorGitlabMergeRequestDiscussionModel();
        discussion->fromJson( object.toObject() );

        if ( discussion->notes().isEmpty() ) {
            delete discussion;
            discussion = nullptr;

            continue;
        }

        response.append( discussion );
    }

    return response;

}

void AthorGitlabMergeRequestDiscussionRequest::updateResolved( const int& projectId, const int& mergeIid, const QString& discussionId, const bool& resolved ) {

    QString url = _url.url();
    url = url.append( "/api/v4/projects/" );
    url = url.append( QString::number( projectId ) );
    url = url.append( "/merge_requests/" );
    url = url.append( QString::number( mergeIid ) );
    url = url.append( "/discussions/" );
    url = url.append( discussionId );
    url = url.append( "?resolved=" );
    url = url.append( resolved ? "true" : "false" );

    _url = QUrl( url );
    AthorGitlabRequest::put( "" );

}

void AthorGitlabMergeRequestDiscussionRequest::insert( const int& projectId, const int& mergeIid, const QString& body ) {

    setRawHeader( "content-type", "application/x-www-form-urlencoded" );

    QString url = _url.url();
    url = url.append( "/api/v4/projects/" );
    url = url.append( QString::number( projectId ) );
    url = url.append( "/merge_requests/" );
    url = url.append( QString::number( mergeIid ) );
    url = url.append( "/discussions" );
    url = url.append( "?body=" );
    url = url.append( body );

    _url = QUrl( url );
    AthorGitlabRequest::post( "" );

}

ATHOR_GITLAB_API_END_NAMESPACE
