#ifndef ATHORGITLABMERGEREQUESTDISCUSSIONREQUEST_H
#define ATHORGITLABMERGEREQUESTDISCUSSIONREQUEST_H

#include <athor-gitlab-api_global.h>

#include <request/athorgitlabrequest.h>
#include <data/athorgitlabmergerequestdiscussionmodel.h>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestDiscussionRequest : public AthorGitlabRequest {
    using AthorGitlabRequest::AthorGitlabRequest;
public:
    QList<AthorGitlabMergeRequestDiscussionModel*> getListByMerge( const int& projectId, const int& mergeIid );

    void updateResolved( const int& projectId, const int& mergeIid, const QString& discussionId, const bool& resolved );

    void insert( const int& projectId, const int& mergeIid, const QString& body );

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTDISCUSSIONREQUEST_H
