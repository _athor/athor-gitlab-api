#include "athorgitlabmergerequestrequest.h"

#include <QJsonObject>
#include <QJsonDocument>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabMergeRequestModel* AthorGitlabMergeRequestRequest::getSingle( const int& projectId, const int& mergeIid ) {

    QString url = _url.url();
    url = url.append( "/api/v4/projects/" );
    url = url.append( QString::number( projectId ) );
    url = url.append( "/merge_requests/" );
    url = url.append( QString::number( mergeIid ) );

    _url = QUrl( url );
    QJsonObject json = AthorGitlabRequest::get().object();

    AthorGitlabMergeRequestModel* response = new AthorGitlabMergeRequestModel();
    response->fromJson( json );

    return response;

}

ATHOR_GITLAB_API_END_NAMESPACE
