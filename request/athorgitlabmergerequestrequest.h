#ifndef ATHORGITLABMERGEREQUESTREQUEST_H
#define ATHORGITLABMERGEREQUESTREQUEST_H

#include <athor-gitlab-api_global.h>

#include <request/athorgitlabrequest.h>
#include <data/athorgitlabmergerequestmodel.h>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestRequest : public AthorGitlabRequest {
    using AthorGitlabRequest::AthorGitlabRequest;
public:
    AthorGitlabMergeRequestModel* getSingle( const int& projectId, const int& mergeIid );

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTREQUEST_H
