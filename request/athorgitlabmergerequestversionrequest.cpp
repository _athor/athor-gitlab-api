#include "athorgitlabmergerequestversionrequest.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

QList<AthorGitlabMergeRequestVersionModel*> AthorGitlabMergeRequestVersionRequest::getMergeDiffVersion( const int& projectId, const int& mergeIid ) {

    QString url = _url.url();
    url = url.append( "/api/v4/projects/" );
    url = url.append( QString::number( projectId ) );
    url = url.append( "/merge_requests/" );
    url = url.append( QString::number( mergeIid ) );
    url = url.append( "/versions" );

    _url = QUrl( url );
    QJsonArray jsonList = AthorGitlabRequest::get().array();

    QList<AthorGitlabMergeRequestVersionModel*> responseList = {};

    for ( QJsonValue json : jsonList ) {
        AthorGitlabMergeRequestVersionModel* response = new AthorGitlabMergeRequestVersionModel();
        response->fromJson( json.toObject() );
        responseList.append( response );
    }

    return responseList;

}

QList<AthorGitlabMergeRequestVersionDiffModel*> AthorGitlabMergeRequestVersionRequest::getSingleMergeDiffVersion( const int& projectId, const int& mergeIid, const int& diffVersionId ) {

    QString url = _url.url();
    url = url.append( "/api/v4/projects/" );
    url = url.append( QString::number( projectId ) );
    url = url.append( "/merge_requests/" );
    url = url.append( QString::number( mergeIid ) );
    url = url.append( "/versions/" );
    url = url.append( QString::number( diffVersionId ) );

    _url = QUrl( url );
    QJsonArray jsonList = AthorGitlabRequest::get().object()["diffs"].toArray();

    QList<AthorGitlabMergeRequestVersionDiffModel*> responseList = {};

    for ( QJsonValue json : jsonList ) {
        AthorGitlabMergeRequestVersionDiffModel* response = new AthorGitlabMergeRequestVersionDiffModel();
        response->fromJson( json.toObject() );
        responseList.append( response );
    }

    return responseList;

}

ATHOR_GITLAB_API_END_NAMESPACE
