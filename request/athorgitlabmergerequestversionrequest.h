#ifndef ATHORGITLABMERGEREQUESTVERSIONREQUEST_H
#define ATHORGITLABMERGEREQUESTVERSIONREQUEST_H

#include <athor-gitlab-api_global.h>

#include <request/athorgitlabrequest.h>
#include <data/athorgitlabmergerequestversionmodel.h>
#include <data/athorgitlabmergerequestversiondiffmodel.h>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabMergeRequestVersionRequest : public AthorGitlabRequest {
    using AthorGitlabRequest::AthorGitlabRequest;
public:
    QList<AthorGitlabMergeRequestVersionModel*> getMergeDiffVersion( const int& projectId, const int& mergeIid );
    QList<AthorGitlabMergeRequestVersionDiffModel*> getSingleMergeDiffVersion( const int& projectId, const int& mergeIid, const int& diffVersionId );

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABMERGEREQUESTVERSIONREQUEST_H
