#include "athorgitlabrequest.h"

#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

ATHOR_GITLAB_API_BEGIN_NAMESPACE

AthorGitlabRequest::AthorGitlabRequest() = default;

AthorGitlabRequest::AthorGitlabRequest( const QString& url, const QString& privateToken ) :
    _url( url ),
    _privateToken( privateToken ) {}

QJsonDocument AthorGitlabRequest::get() {
    return send([this]( QNetworkAccessManager& acessManager ) {
        return acessManager.get( *this );
    } );
}

QJsonDocument AthorGitlabRequest::put( const QByteArray& data ) {
    return send([&]( QNetworkAccessManager& acessManager ) {
        return acessManager.put( *this, data );
    } );
}

QJsonDocument AthorGitlabRequest::post( const QByteArray& data ) {
    return send([&]( QNetworkAccessManager& acessManager ) {
        return acessManager.post( *this, data );
    } );
}

int AthorGitlabRequest::timeout() const {
    return _timeout;
}

void AthorGitlabRequest::setTimeout( int timeout ) {
    _timeout = timeout;
}

void AthorGitlabRequest::setUrl( const QString& url ) {
    _url = QUrl( url );
}

QString AthorGitlabRequest::privateToken() const {
    return _privateToken;
}

void AthorGitlabRequest::setPrivateToken( const QString& privateToken ) {
    _privateToken = privateToken;
}

QJsonDocument AthorGitlabRequest::send( const std::function<QNetworkReply*(QNetworkAccessManager&)>& exec ) {

    qDebug() << "AthorGitlabRequest::send [URL]" << _url;

    QJsonDocument json;
    QNetworkAccessManager accessManager;

    QNetworkRequest::setUrl( _url );
    QNetworkRequest::setRawHeader( "PRIVATE-TOKEN", _privateToken.toUtf8() );

    QNetworkReply* reply = exec( accessManager );

    QTimer singleShot;
    singleShot.setSingleShot( true );

    QEventLoop eventLoop;
    QObject::connect( &singleShot, &QTimer::timeout, &eventLoop, &QEventLoop::quit );
    QObject::connect( &accessManager, &QNetworkAccessManager::finished, &eventLoop, &QEventLoop::quit );
    singleShot.start( _timeout );
    eventLoop.exec();

    if ( singleShot.isActive() ) {
        qDebug() << "AthorGitlabRequest::send finished";
        singleShot.stop();

        if ( reply->error() == QNetworkReply::NoError ) {
            qDebug() << "AthorGitlabRequest::send ok";
            json = QJsonDocument::fromJson( reply->readAll() );
            delete reply;

        } else {
            QString error = reply->errorString();
            qCritical() << "AthorGitlabRequest::send [ERROR]" << error;
            delete reply;

            throw std::runtime_error( error.toStdString().c_str() );
        }

    } else {
        qCritical() << "AthorGitlabRequest::send timeout";
        QObject::disconnect( &singleShot, &QTimer::timeout, &eventLoop, &QEventLoop::quit );

        reply->abort();
        QString error = reply->errorString();
        delete reply;

        throw std::runtime_error( error.toStdString().c_str() );
    }

    return json;
}

ATHOR_GITLAB_API_END_NAMESPACE
