#ifndef ATHORGITLABREQUEST_H
#define ATHORGITLABREQUEST_H

#include <functional>

#include <QUrl>
#include <QNetworkRequest>

#include <athor-gitlab-api_global.h>

class QJsonDocument;
class QNetworkReply;
class QNetworkAccessManager;

ATHOR_GITLAB_API_BEGIN_NAMESPACE

class AthorGitlabRequest : public QNetworkRequest {
    using QNetworkRequest::QNetworkRequest;
public:
    AthorGitlabRequest();
    AthorGitlabRequest( const QString& url, const QString& privateToken );

    QJsonDocument get();
    QJsonDocument put( const QByteArray& data );
    QJsonDocument post( const QByteArray& data );

    int timeout() const;
    void setTimeout( int timeout );

    void setUrl( const QString& url );

    QString privateToken() const;
    void setPrivateToken( const QString& privateToken );

protected:
    QUrl _url;

    QJsonDocument send( const std::function<QNetworkReply*( QNetworkAccessManager& acessManager )>& exec );

private:
    int _timeout = 10000;
    QString _privateToken = "";

};

ATHOR_GITLAB_API_END_NAMESPACE

#endif // ATHORGITLABREQUEST_H
